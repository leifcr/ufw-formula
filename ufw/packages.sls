fail2ban:
  pkg:
    - latest

python-pip:
  pkg.installed:
    - pkgs:
      - python-pip

ufw-iptools-pip-package:
  pip.installed:
    - name: iptools
    - require:
      - pkg: python-pip

refresh_ufw_modules:
  module.run:
    - name: saltutil.sync_modules
    - refresh: True
    - require_in:
      - pkg: ufw

ufw:
  pkg:
    - installed
  ufw.enabled:
    - require:
      - pkg: ufw
      - pkg: fail2ban
      - pip: ufw-iptools-pip-package
