#
# Based on info in this blog post:
# http://blog.publysher.nl/2013/08/infra-as-repo-securing-your.html
#
# With minor modifications to support latest saltstack version
# And additional commands
#
"""
Execution module for UFW.
"""
def is_enabled():
    out = __salt__['cmd.run']('ufw status')
    return True if out.find('Status: active') > -1 else False

def set_enabled(enabled):
    cmd = 'ufw --force enable' if enabled else 'ufw disable'
    __salt__['cmd.run'](cmd)

def add_rule(rule):
    cmd = "ufw " + rule
    out = __salt__['cmd.run'](cmd)
    __salt__['cmd.run']("ufw reload")
    return out

def status():
    out = __salt__['cmd.run']('ufw status numbered')
    return out

def delete(num):
    out = __salt__['cmd.run']('ufw --force delete ' + str(num))
    return out

def reset():
    out = __salt__['cmd.run']('ufw --force reset')
    return out
